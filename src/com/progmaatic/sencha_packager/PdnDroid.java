package com.progmaatic.sencha_packager;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

public class PdnDroid extends Activity {

	// PRIVATE FIELDS
	LinearLayout rootLayout;
	WebView webView;
	ImageView splashView;
	String siteUrl;
	int imageResource;
	int splashDelay;

	// CONSTANTS
	public static final int FULLSCREEN = android.R.style.Theme_Translucent_NoTitleBar_Fullscreen;
	public static final int NO_TITLEBAR = android.R.style.Theme_Translucent_NoTitleBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		rootLayout = new LinearLayout(this);
		rootLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT, 0.0F));
		rootLayout.setOrientation(LinearLayout.VERTICAL);

		webView = new WebView(this);
		WebSettings settings = webView.getSettings();
		settings.setJavaScriptEnabled(true);
		webView.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0F));
		rootLayout.addView(webView);

		imageResource = -1;
		splashView = new ImageView(this);
		splashView.setScaleType(ScaleType.FIT_XY);
	}

	// ENTRY POINT OF THE APP
	protected void startApp() {

		// IF NO SPLASH-SCREEN
		if (imageResource < 0) {
			//rootLayout.addView(webView);
			setContentView(rootLayout);
			this.loadUrl(siteUrl);
		}

		// IF THERE IS SPLASH-SCREEN
		else {
			setContentView(splashView);
			loadUrl(siteUrl);
			Handler h = new Handler();
			h.postDelayed(new Runnable() {
				@Override
				public void run() {
					//rootLayout.addView(webView);
					setContentView(rootLayout);
				}
			}, splashDelay);
		}
	}

	// SET THE MAIN URL
	protected void setSiteUrl(String url) {
		siteUrl = url;
	}

	// LOAD URL IN THE WEBVIEW
	protected void loadUrl(String url) {
		webView.loadUrl(url);
	}

	// SET THE SPLASH-SCREEN IMAGE AND SPLASH-DELAY
	protected void setSplashScreen(int imageResource, int millisecond) {
		this.imageResource = imageResource;
		splashView.setImageResource(this.imageResource);
		splashDelay = millisecond;
	}

	@Override
	public void onBackPressed() {
		if (webView.canGoBack()) {
			webView.goBack();
		} else {
			Log.e("Back-Press", "Exiting app");
			super.onBackPressed();
		}
	}

	@Override
	public void setTheme(int resid) {
		super.setTheme(resid);
	}
	
	protected void addViewToScreen (View view) {
		rootLayout.addView(view);
	}

}
